
# Esper

### Prerequisites

- Install [Java JDK](https://jdk.java.net/) 11 or higher

- Downlad [Eclipse](https://www.eclipse.org/downloads/) and install Eclipse IDE for Java Developers

- Download [Esper 8.7.0](https://www.espertech.com/esper/esper-downloads/) 
 
- Clone or Download this repository ([clone command using git](https://git-scm.com/docs/git-clone))

### Setup the project

**Import the Project**
- Start Eclipse
- File -> Import -> Maven -> Existing Maven Projects
- Set the Root Directory to the EsperExercise directory of this repository

**Make sure the Project uses Java Compiler 1.8**
- Project -> Properties -> Java Compiler
- if needed untick 'Use compliance from execution environment...'
- Set Compiler compliance level to 1.8

**Configure the Build Path**
- Right Click on the Project -> Build Path -> Configure Build Path
- Add External JARs... -> choose all .jar files from the Dependencies directory of this repository
- Add External JARs... -> choose all .jar files from the downloaded Esper 8.7.0 directory
- if the used JRE System Library is <1.8 :
    - Remove the JRE System Library
    - Add Library... -> JRE System Library -> Execution Environment: JavaSE-1.8 -> Finish

____

# Working with Esper


## 1. Introductionary Example

*lecture.example* contains an Esper example to get to know the architecture and structure of Esper.

This example processes a stream of *PersonBean*s, which are objects containing an indentification number, a name and age.

There are two EPL (Event Processing Language) statements:

- The *MonitoringEPL* looks for events/PersonBeans with an age of 35 and higher.
    ``` EPL = "select * from PersonBean (age > 35)"; ```

    The *MonitoringEventListener* is added. Everytime the MonitoringEPL finds a matching event, the listener processes it. In this case it logs the event with name and age.
    ``` 
    logEvent(EventBean event) {
        String name = (String) event.get("name");
        int age = (int) event.get("age");
        log.info("Name: " + name + ", Age: " + age); 
    ```

- The *MonitoringConsecutiveEPL* looks for a pattern. The pattern defines two consecutive events/PersonBeans that share the same id.
    ```
    EPL = "select * from PersonBean "
		+ " match_recognize  ( " 
		+ " measures A as Person1, B as Person2" 
		+ " pattern (A B)" 
		+ " define " 
		+ "  B as B.id = A.id )";
    ```
    A listener of type *MonitoringConsecutiveEventListener* is added. If the given pattern is found, the listener processes the event. This event contains both PersonBeans that were matched in the pattern. Because the id's are the same in this case it contains the same PersonBean twice. The Id of the PersonBeans is logged.


The *ExampleMain* sets everything up.
Six Events are sent. Five of them will be found by the MonitoringEPL (Event 1, 2, 3, 4 and 6), and two consecutive Events with the same Id will be found once by the MonitoringConsecutiveEPL (Event 3 and 4), resulting in the following output:

```
Starting..
Initializing runtime URI 'EsperExample' version 8.7.0
Send event 1
 ~~Event~~
 Name: Bob, Age: 38
Send event 2
 ~~Event~~
 Name: Otto Normal, Age: 49
Send event 3
 ~~Event~~
 Name: Alice, Age: 53
Send event 4
 ~~Event~~
 Name: Alice, Age: 53
 ~~Consecutive Event~~
 Two consecutive Events with ID 3
Send event 5
Send event 6
 ~~Event~~
 Name: James, Age: 73
```


______

## 2. Exercise

[Helpful patterns](https://www.espertech.com/esper/solution-patterns/) and the  [Esper Reference](http://esper.espertech.com/release-8.7.0/reference-esper/html/) for support.



A handful of different sensors provide different data that you can process.
There are
- A temperature sensor that delivers a new measurement every two seconds consisting of a time stamp, the measured temperature and the location of the measurement.
The location has two possible values:
    - OFFICE
    - HALLWAY
- Three pressure sensors that provide new measurements at random times. Each measurement consists of a sensorId that identifies the sensor of this measurement, the measured pressure and a status. 
The status has one of three possible values: 
    - EVENT_TRIGGERED, the sensor was triggered by a employee to send the current measurement to your system.
    - EVENT_MONITORED, the sensor was not triggered in a while and automatically sends its current measurement to your system.
    - EVENT_CORRUPTED, the sensor was either triggered or executed a monitoring measurement, but due to other factors can not guarantee that the measured pressure is correct.

The Events by the Sensors are generated dynamically via the *EventGenerator*. Process them in the following ways:

### 1. Write EPL statements and listeners to process the Temperature Events in the following ways:

a)  Get the average temperature every ten seconds. What is the difference in using  ```time``` and ```time_batch```?

b) For each location regularly output the maximum and minimum temperature ever sent.

c) Look for three consecutive Temperature Events with a temperature higher than 100 degree.

d)   Calculate the Temperature Events per 5 seconds for each location, and save them in a [named window](http://esper.espertech.com/release-5.3.0/esper-reference/html/nwtable.html#named_inserting). Output the current count for each location. 

Make the window available for other EPLs by setting the NameAccessModifier to public:
```
CompilerArguments args = new CompilerArguments(config);
args.getOptions().setAccessModifierEventType(env -> NameAccessModifier.PUBLIC);
EPCompiled compiled = EPCompilerProvider.getCompiler().compile(EPL, args);
```

The windows is accessed by other EPLs by configuring the Compiler Arguments:
```
CompilerArguments args = new CompilerArguments(config);
args.getPath().add(runtime.getRuntimePath());
``` 

e) Detect when the event count per 5 seconds falls below the average number of events per 5 seconds over the last 20 seconds.

### 2. Write EPL statements to process the Pressure Events in the following ways:

a) Count the number of triggered Pressure Events for each 10 seconds for each Sensor.

b) For each sensor get the average pressure of the last 15 seconds. Only consider measurements that are definitely correct. Make this data available to your listener every five seconds ([hint](http://esper.espertech.com/release-8.7.0/reference-esper/html/epl_clauses.html#epl-output-rate)), sorted from highest to lowest average pressure. 

c) Create a [Pattern](http://esper.espertech.com/release-8.7.0/reference-esper/html/event_patterns.html#pattern-how-to-use) to match low Pressure Events (Pressure lower than 8) and high Pressure Events (Pressure higher than 42). Assign tags to identify in your Listener whether you matched a high or low event and output that information accordingly.
