package com.esper.lecture.example;

public class PersonBean {

	private int Id;
	private String name;
	private int age;

	public PersonBean(int Id, String name, int age) {
			this.Id = Id;
		    this.name = name;
		    this.age = age;
		  }
	
	public int getId() {
		return Id;
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}
}
