package com.esper.lecture.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.espertech.esper.common.client.configuration.Configuration;
import com.espertech.esper.compiler.client.EPCompileException;
import com.espertech.esper.runtime.client.EPDeployException;
import com.espertech.esper.runtime.client.EPRuntime;
import com.espertech.esper.runtime.client.EPRuntimeDestroyedException;
import com.espertech.esper.runtime.client.EPRuntimeProvider;

public class ExampleMain {
	private static final Logger log = LoggerFactory.getLogger(ExampleMain.class);

	private final String runtimeURI;

	public static void main(String[] args) throws EPRuntimeDestroyedException, EPCompileException, EPDeployException, InterruptedException {
		log.info("Starting..");
		new ExampleMain("EsperExample").run();
	}

	public ExampleMain(String runtimeURI) {
		this.runtimeURI = runtimeURI;
	}

	public void run() throws EPCompileException, EPRuntimeDestroyedException, EPDeployException, InterruptedException {
        // Configuration object
		// add preconfigured event type PersonBean
		Configuration configuration = new Configuration();
        configuration.getCommon().addEventType(PersonBean.class);
        
        // Runtime
        // provides environment to execute and compile modules
        EPRuntime runtime = EPRuntimeProvider.getRuntime(runtimeURI, configuration);
        runtime.initialize();
        
        // Set up statements
        MonitoringEPL monitoringStmt = new MonitoringEPL(runtime, configuration);
        monitoringStmt.addListener(new MonitoringEventListener());
  
        MonitoringConsecutiveEPL monitoringConsStmnt = new MonitoringConsecutiveEPL(runtime, configuration);
        monitoringConsStmnt.addListener(new MonitoringConsecutiveEventListener());
 
        // Send Events
        int count = 1;
        log.info("Send event " + count++);
        runtime.getEventService().sendEventBean(new PersonBean(1, "Bob", 38), MonitoringEPL.EVENTTYPE);
        log.info("Send event " + count++);
        runtime.getEventService().sendEventBean(new PersonBean(2, "Otto Normal", 49), MonitoringEPL.EVENTTYPE);
        log.info("Send event " + count++);
        runtime.getEventService().sendEventBean(new PersonBean(3, "Alice", 53), MonitoringEPL.EVENTTYPE);
        log.info("Send event " + count++);
        runtime.getEventService().sendEventBean(new PersonBean(3, "Alice", 53), MonitoringEPL.EVENTTYPE);
        log.info("Send event " + count++);
        runtime.getEventService().sendEventBean(new PersonBean(4, "Daisy", 22), MonitoringEPL.EVENTTYPE);
        log.info("Send event " + count);
        runtime.getEventService().sendEventBean(new PersonBean(5, "James", 73), MonitoringEPL.EVENTTYPE);
  
	}

}
