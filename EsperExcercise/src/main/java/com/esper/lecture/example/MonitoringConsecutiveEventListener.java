package com.esper.lecture.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.espertech.esper.common.client.EventBean;
import com.espertech.esper.runtime.client.EPRuntime;
import com.espertech.esper.runtime.client.EPStatement;
import com.espertech.esper.runtime.client.UpdateListener;

public class MonitoringConsecutiveEventListener implements UpdateListener {

	@Override
	public void update(EventBean[] newEvents, EventBean[] oldEvents, EPStatement statement, EPRuntime runtime) {
		if (newEvents != null) {
			// Process the new Event
			logEvent(newEvents[0]);
		}
	}
	
	// A pattern of two Events was matched
	// the matchEvent contains both matched Events
	public void logEvent(EventBean matchEvent) {
		PersonBean person = (PersonBean) matchEvent.get("Person1");
		int Id = person.getId();
        log.info("~~Consecutive Event~~");
        log.info("Two consecutive Events with ID " + Id);
	}

    private static final Logger log = LoggerFactory.getLogger(MonitoringConsecutiveEventListener.class);
    
}
