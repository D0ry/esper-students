package com.esper.lecture.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.espertech.esper.common.client.EventBean;
import com.espertech.esper.runtime.client.EPRuntime;
import com.espertech.esper.runtime.client.EPStatement;
import com.espertech.esper.runtime.client.UpdateListener;

public class MonitoringEventListener implements UpdateListener {

	public void update(EventBean[] newEvents, EventBean[] oldEvents, EPStatement statement, EPRuntime runtime) {
		if (newEvents != null) {
			// Process the new Event
			logEvent(newEvents[0]);
		}
	}
	
	// Variables of the Event can be accessed by their names
	// This case:
	// 		"select * from PersonBean"
	// 		access via the variable names of the PersonBean Object
	//		event.get("name")
	// Other example:
	// 		"select age as AgePerson, name as NamePerson from PersonBean"
	//		access via assigned names
	//		event.get("NamePerson")
	public void logEvent(EventBean event) {
        String name = (String) event.get("name");
        int age = (int) event.get("age");
        
        log.info("~~Event~~");
        log.info("Name: " + name + ", Age: " + age);
	}

    private static final Logger log = LoggerFactory.getLogger(MonitoringEventListener.class);

}
