package com.esper.lecture.example;

import com.espertech.esper.common.client.EPCompiled;
import com.espertech.esper.common.client.configuration.Configuration;
import com.espertech.esper.compiler.client.CompilerArguments;
import com.espertech.esper.compiler.client.EPCompilerProvider;
import com.espertech.esper.runtime.client.EPRuntime;
import com.espertech.esper.runtime.client.EPStatement;

public class MonitoringEPL {
	
	// Incoming Events are of Type PersonBean
	final static String EVENTTYPE ="PersonBean";
	
	// EPL filters Events to look for Events with age > 35
	final static String EPL = "select * from PersonBean (age > 35)";

	private EPStatement statement;
	
	public MonitoringEPL(EPRuntime runtime, Configuration config) {
		try {
			// Compile the Module
			EPCompiled compiled = EPCompilerProvider.getCompiler().compile(EPL, new CompilerArguments(config));
			// Deploy the compiled Module
			statement = runtime.getDeploymentService().deploy(compiled).getStatements()[0];
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// Add a Listener that processes the Events
	public void addListener(MonitoringEventListener listener) {
		statement.addListener(listener);
	}
}
