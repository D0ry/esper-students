package com.esper.lecture.example;

import com.espertech.esper.common.client.EPCompiled;
import com.espertech.esper.common.client.configuration.Configuration;
import com.espertech.esper.compiler.client.CompilerArguments;
import com.espertech.esper.compiler.client.EPCompilerProvider;
import com.espertech.esper.runtime.client.EPRuntime;
import com.espertech.esper.runtime.client.EPStatement;

public class MonitoringConsecutiveEPL {
	
	// Incoming Events are of Type PersonBean
	final static String EVENTTYPE ="PersonBean";
	
	// EPL filters two consecutive Events with the same ID
	final static String EPL = "select * from PersonBean"
			+ " match_recognize  ( " 
			+ " measures A as Person1, B as Person2" 
			+ " pattern (A B)" 
			+ " define " 
			+ "  B as B.id = A.id )";
	
	private EPStatement statement;
	
	public MonitoringConsecutiveEPL(EPRuntime runtime, Configuration config) {
		try {
			// Compile the Module
			EPCompiled compiled = EPCompilerProvider.getCompiler().compile(EPL, new CompilerArguments(config));
			// Deploy the compiled Module
			statement = runtime.getDeploymentService().deploy(compiled).getStatements()[0];
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// Add a Listener that processes the Events
	public void addListener(MonitoringConsecutiveEventListener listener) {
		statement.addListener(listener);
	}
}
