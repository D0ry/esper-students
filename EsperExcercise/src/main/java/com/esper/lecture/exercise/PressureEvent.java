package com.esper.lecture.exercise;

public class PressureEvent {
	
	private double pressure;

	private int sensorId;
	
	private EventStatus status;
	
	public PressureEvent (double pressure, int id, EventStatus status) {
		this.pressure = pressure;
		this.sensorId = id;
		this.status = status;
	}
	
	public double getPressure() {
		return pressure;
	}
	
	public int getSensorId() {
		return sensorId;
	}
	
	public EventStatus getStatus() {
		return status;
	}
	
}
