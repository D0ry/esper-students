package com.esper.lecture.exercise;

import java.util.Date;

public class TemperatureEvent {
	
	private int temperature;
	
	private Date date;
	
	private Location location;
	
	public TemperatureEvent(int temperature, Date date, Location location) {
		this.temperature = temperature;
		this.date = date;
		this.location = location;
	}
	
	public int getTemperature() {
		return temperature;
	}

	public Date getDate() {
		return date;
	}
	public Location getLocation() {
		return location;
	}
}
