package com.esper.lecture.exercise;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.espertech.esper.common.client.EventBean;
import com.espertech.esper.runtime.client.EPRuntime;
import com.espertech.esper.runtime.client.EPStatement;
import com.espertech.esper.runtime.client.UpdateListener;

public class AverageTemperatureListener implements UpdateListener{
	

	public void update(EventBean[] newEvents, EventBean[] oldEvents, EPStatement statement, EPRuntime runtime) {
		logEvent(newEvents[0]);
	}
	
	public void logEvent(EventBean event) {
		// Log the Event
		log.info("");
	}
	
    private static final Logger log = LoggerFactory.getLogger(AverageTemperatureListener.class);
}
