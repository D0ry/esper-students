package com.esper.lecture.exercise;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.esper.lecture.example.ExampleMain;
import com.espertech.esper.common.client.configuration.Configuration;
import com.espertech.esper.runtime.client.EPRuntime;
import com.espertech.esper.runtime.client.EPRuntimeProvider;

public class ExerciseMain {
	private static final Logger log = LoggerFactory.getLogger(ExampleMain.class);

	private final String runtimeURI;

	public static void main(String[] args) throws InterruptedException {
		log.info("Starting..");
		new ExerciseMain("EsperExercise").run();
	}

	public ExerciseMain(String runtimeURI) {
		this.runtimeURI = runtimeURI;
	}

	public void run() throws InterruptedException {
		// Add The Event Types
		Configuration configuration = new Configuration();
		configuration.getCommon().addEventType(TemperatureEvent.class);
		configuration.getCommon().addEventType(PressureEvent.class);

		EPRuntime runtime = EPRuntimeProvider.getRuntime(runtimeURI, configuration);
		runtime.initialize();

		// YOUR CODE BELOW
		// Set up Your Statements
		
		// Exercise 1.a
		AverageTemperatureEPL avgTempStmt = new AverageTemperatureEPL(runtime, configuration);
		avgTempStmt.addListener(new AverageTemperatureListener());

		// ...
		
		// YOUR CODE ABOVE

		
		// Event Generation
		Thread tempThread = new Thread(() -> {
			try {
				EventGenerator.sendTempEvents(runtime, false);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		});
		
		Thread presThread = new Thread(() -> {
			try {
				EventGenerator.sendPressureEvents(runtime, false);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		});
		
		tempThread.start();
		presThread.start();
	}
}
