package com.esper.lecture.exercise;

public enum EventStatus {
	EVENT_TRIGGERED,
	EVENT_MONITORING,
	EVENT_CORRUPTED
}
