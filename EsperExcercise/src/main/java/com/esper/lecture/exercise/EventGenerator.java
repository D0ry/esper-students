package com.esper.lecture.exercise;

import java.util.Date;
import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.espertech.esper.runtime.client.EPRuntime;

public class EventGenerator {
	private static final Logger log = LoggerFactory.getLogger(EventGenerator.class);
	
	public static void sendTempEvents(EPRuntime runtime, boolean output) throws InterruptedException {

		Date datea = new Date();
		TemperatureEvent tempEvent1 = new TemperatureEvent(200, datea, Location.HALLWAY);
		runtime.getEventService().sendEventBean(tempEvent1, "TemperatureEvent");
		tempEvent1 = new TemperatureEvent(200, datea, Location.HALLWAY);
		runtime.getEventService().sendEventBean(tempEvent1, "TemperatureEvent");
		tempEvent1 = new TemperatureEvent(200, datea, Location.OFFICE);
		runtime.getEventService().sendEventBean(tempEvent1, "TemperatureEvent");
		
		for (int i = 0; i < 40; i++) {
			int temp = new Random().nextInt(300);
			Date date = new Date();
			
			int rand = new Random().nextInt(2); 				
			Location randomlocation = Location.values()[rand];

			if (output) log.info("Sending Temp Event:  Locations " + randomlocation + ", temp " + temp);

			TemperatureEvent tempEvent = new TemperatureEvent(temp, date, randomlocation);
			runtime.getEventService().sendEventBean(tempEvent, "TemperatureEvent");
			Thread.sleep(1000);
		}
		log.info("All Temperatures Events sent");
	}
	
	public static void sendPressureEvents(EPRuntime runtime, boolean output) throws InterruptedException {
		for (int k = 0; k < 40; k++) {
			double pressure = new Random().nextInt(50) + new Random().nextDouble();
			
			int rand = new Random().nextInt(3); 				
			EventStatus randomStatus = EventStatus.values()[rand];
			
			if (output) log.info("Sending Pressure Event: Id " + (k%3+1) + ", pressure " + pressure + ", status " + randomStatus);
			
			PressureEvent presEvent = new PressureEvent(pressure, k%3+1, randomStatus);
			runtime.getEventService().sendEventBean(presEvent, "PressureEvent");
			Thread.sleep(new Random().nextInt(2000));
		}
		log.info("All Pressure Events sent");
	}

}
