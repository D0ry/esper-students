package com.esper.lecture.exercise;

import com.espertech.esper.common.client.EPCompiled;
import com.espertech.esper.common.client.configuration.Configuration;
import com.espertech.esper.compiler.client.CompilerArguments;
import com.espertech.esper.compiler.client.EPCompilerProvider;
import com.espertech.esper.runtime.client.EPRuntime;
import com.espertech.esper.runtime.client.EPStatement;

public class AverageTemperatureEPL {
	
	// add the EPL
	final static String EPL = "";

	private EPStatement statement;

	public AverageTemperatureEPL(EPRuntime runtime, Configuration config) {
		try {
			EPCompiled compiled = EPCompilerProvider.getCompiler().compile(EPL, new CompilerArguments(config));
			statement = runtime.getDeploymentService().deploy(compiled).getStatements()[0];
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void addListener(AverageTemperatureListener listener) {
		statement.addListener(listener);
	}
}
